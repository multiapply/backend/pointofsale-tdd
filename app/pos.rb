class Display
  attr_accessor :text
end


class Sale
  attr :prices_by_barcode
  
  def initialize(display, prices_by_barcode)
    @display = display
    @prices_by_barcode = prices_by_barcode
    
  end

  def on_barcode(barcode)
    if barcode.eql? ""
      @display.text = "Scanning error: empty barcode"
      return
    end
    
    if @prices_by_barcode.key? barcode
      @display.text = @prices_by_barcode[barcode]
    else
      @display.text = "Product not found for #{barcode}"
    end
  end
end