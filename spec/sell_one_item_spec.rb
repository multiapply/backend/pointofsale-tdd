require_relative '../app/pos'
describe "Sell One Item" do
  before do
    @display = Display.new
    @sale = Sale.new(@display,
      {
        "12345" => "$7.95",
        "23456" => "$12.50",
      }
    )
  end

  describe "Find a Product" do
    
    it "Product Found" do
      @sale.on_barcode("12345")
  
      expect(@display.text).to eq "$7.95"
    end
  
    it "Another Product Found" do
      @sale.on_barcode("23456")
  
      expect(@display.text).to eq "$12.50"
    end
  
    it "Product not Found" do
      @sale.on_barcode("99999")
  
      expect(@display.text).to eq "Product not found for 99999"
    end  
  end

  describe "Barcode Result" do
    it "Empty Barcode" do
      sale = Sale.new(@display,nil)
      
      sale.on_barcode("")
  
      expect(@display.text).to eq "Scanning error: empty barcode" 
    end
    
  end
  
  
  
end
